﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class Form1 : Form
    {
        [DllImport("VTKNet.dll", EntryPoint = "initWindow", CharSet = CharSet.Ansi)]
        public extern static bool initWindow(IntPtr parent);

        [DllImport("VTKNet.dll", EntryPoint = "destroyWindow", CharSet = CharSet.Ansi)]
        public extern static bool destroyWindow();

        public Form1()
        {
            InitializeComponent();
            initWindow(this.Handle);
        }
    }
}
