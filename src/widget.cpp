#include "widget.h"
#include "ui_widget.h"
#include <vtkAutoInit.h>
#include <vtkSTLReader.h>
#include <vtkPolyDataMapper.h>

VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    initViewer();


    //测试
    std::string file =  "C:\\Users\\keiler\\Desktop\\file\\you-hou-che-men.stl";
    vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(file.c_str());
    reader->Update();

    vtkSmartPointer<vtkPolyDataMapper> mapper =    vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(reader->GetOutputPort());

    vtkNew<vtkActor> partActor;
    partActor->SetMapper(mapper);
    Renderer->AddActor(partActor);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::initViewer()
{
    RenderWindow=vtkSmartPointer<vtkRenderWindow>::New();

    Renderer = vtkSmartPointer<vtkRenderer>::New();
    Renderer->SetBackground(0.2, 0.2, 0.2);
    Renderer->SetUseDepthPeeling(true);
    Renderer->SetOcclusionRatio(0.1);
    RenderWindow->AddRenderer(Renderer);
    ui->widget->SetRenderWindow(RenderWindow);
}
