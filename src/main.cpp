#include "widget.h"
#include <QApplication>

#include <windows.h>
#include <qmfcapp.h>
#include <qwinwidget.h>

// int main(int argc, char *argv[])
// {
//     QApplication a(argc, argv);
//     Widget w;
//     w.show();
//     return a.exec();
// }

BOOL WINAPI DllMain( HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpvReserved*/ )
{
    static bool ownApplication = FALSE;

    if ( dwReason == DLL_PROCESS_ATTACH )
        ownApplication = QMfcApp::pluginInstance( hInstance );
    if ( dwReason == DLL_PROCESS_DETACH && ownApplication )
        delete qApp;

    return TRUE;
}

QWinWidget *win=nullptr;
extern "C" __declspec(dllexport) bool initWindow( HWND parent )
{
    if(parent==nullptr)
        return false;
    win = new QWinWidget(parent);
    Widget *widget = new Widget(win);
    widget->show();
    win->move(0,0);
    win->show();

    return TRUE;
}

extern "C" __declspec(dllexport) bool destroyWindow()
{
    if(win!=0){
        win->close();
        delete win;
    }

    return TRUE;
}
