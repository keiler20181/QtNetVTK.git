#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPoints.h>
#include <vtkCellLocator.h>
#include <vtkProperty.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkTextActor.h>

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;

    vtkSmartPointer<vtkRenderer> Renderer;
    vtkSmartPointer<vtkRenderWindow> RenderWindow;

private:
    void initViewer();
};
#endif // WIDGET_H
