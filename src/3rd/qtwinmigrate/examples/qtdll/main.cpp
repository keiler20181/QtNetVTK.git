// Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
// SPDX-License-Identifier: BSD-3-Clause

#include <qmfcapp.h>
#include <qwinwidget.h>
#include <QLabel>
#include <QMessageBox>
#include <windows.h>

BOOL WINAPI DllMain( HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpvReserved*/ )
{
    static bool ownApplication = FALSE;

    if ( dwReason == DLL_PROCESS_ATTACH )
	ownApplication = QMfcApp::pluginInstance( hInstance );
    if ( dwReason == DLL_PROCESS_DETACH && ownApplication )
	delete qApp;

    return TRUE;
}

QWinWidget *win;
extern "C" __declspec(dllexport) bool showDialog( HWND parent )
{

    win = new QWinWidget(parent);
    QLabel* label = new QLabel(win);
    label->setText("ddddddddd");
    win->move(0,0);
    win->show();
    //win.showCentered();
    //QMessageBox::about( &win, "About QtMfc", "QtMfc Version 1.0\nCopyright (C) 2003" );

    return TRUE;
}
