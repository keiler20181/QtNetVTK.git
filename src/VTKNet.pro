TEMPLATE = lib
CONFIG += warn_off

QT       += core gui
CONFIG += c++17 dll

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
include($$PWD/3rd/qtwinmigrate/src/qtwinmigrate.pri)

SOURCES += \
    main.cpp \
    widget.cpp

HEADERS += \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


INCLUDEPATH += $$PWD/3rd/VTK_8.2/include/vtk-8.2
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkChartsCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonColor-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonComputationalGeometry-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonDataModel-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonExecutionModel-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonMath-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonMisc-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonSystem-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkCommonTransforms-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkDICOMParser-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkDomainsChemistry-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkDomainsChemistryOpenGL2-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkexpat-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersAMR-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersExtraction-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersFlowPaths-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersGeneral-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersGeneric-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersGeometry-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersHybrid-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersHyperTree-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersImaging-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersModeling-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersParallel-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersParallelImaging-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersProgrammable-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersSelection-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersSMP-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersSources-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersStatistics-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersTexture-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkFiltersVerdict-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkfreetype-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkGeovisCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkglew-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkGUISupportQt-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkGUISupportQtSQL-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkhdf5-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkhdf5_hl-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingColor-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingFourier-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingGeneral-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingHybrid-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingMath-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingMorphological-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingSources-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingStatistics-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkImagingStencil-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkInfovisCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkInfovisLayout-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkInteractionImage-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkInteractionStyle-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkInteractionWidgets-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOAMR-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOEnSight-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOExodus-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOExport-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOGeometry-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOImage-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOImport-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOInfovis-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOLegacy-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOLSDyna-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOMINC-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOMovie-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIONetCDF-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOParallel-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOParallelXML-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOPLY-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOSQL-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOVideo-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOXML-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkIOXMLParser-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkjpeg-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkjsoncpp-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtklibxml2-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkmetaio-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkNetCDF-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkParallelCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkpng-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkproj-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingAnnotation-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingContext2D-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingContextOpenGL2-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingFreeType-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingImage-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingLabel-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingLOD-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingOpenGL2-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingQt-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingVolume-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkRenderingVolumeOpenGL2-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtksqlite-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtksys-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtktiff-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkverdict-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkViewsContext2D-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkViewsCore-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkViewsInfovis-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkViewsQt-8.2.lib
LIBS +=$$PWD/3rd/VTK_8.2/lib/vtkzlib-8.2.lib
